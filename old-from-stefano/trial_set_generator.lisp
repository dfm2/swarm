;; reads a set of many configurations from large_set_permuted.csv and runs the model over them
;; the training is always performed with the same 10 instances
;; the model outputs a complete log in actr_log.csv
;; the dataset covers completely the x-y space (10 instances for each cell in a 101x101 grid), the average model response for rv and dp of each cell is reported in the file training_set.csv (this is the markov model training set)

;; Usage:
;; (defun generate-trial-set "/Users/ACTR/Documents/ITA/swarm/trial_set_generation/")

(defparameter *runs* 50)                ;how many times to rerun the exp on the same data
(defparameter *exp-length* 100)         ;how many trials in one experiment (+ 10 trainings)
(defparameter *tot-trials* 0)           ;counts how many trials have been processed
(defparameter *record* nil)
(defparameter *outputs* nil)
(defparameter *samples* nil)
(defparameter *experiment* nil)
(defparameter *experiment-rv* nil)
(defparameter *experiment-dp* nil)

(defparameter *training_variance* (list
"-57.03373327528257	108.13087898530961	15862.632913666295	58.647823612699625	-60.793057227368806	15290.132838589296	0.23553719008264462	0.17975206611570249"
"45.93574199369891	65.18637708844034	19655.23517905769	93.82063849110173	-50.56044824659632	5415.1191612899465	0.256198347107438	0.13223140495867769"
"46.98458388781967	-84.50253616755111	6422.407655426464	-34.15522842571828	-79.65499594158088	16795.692398827974	0.1962809917355372	0.25"
"138.61890886082818	-19.850473140327125	11647.219522051953	-85.71788305762254	-4.205454130091283	15661.519149759057	0.16942148760330578	0.19214876033057851"
"-109.45300036671443	118.35804661650565	13789.377837130109	-102.11258293817338	-72.83488710404868	19047.6113086966	0.17768595041322313	0.18801652892561985"
"-50.08657928601161	-86.18168788592962	11021.418954980842	-11.256087275294476	139.25990816080298	3987.877510045318	0.24380165289256198	0.2024793388429752"
"-81.05084481871168	-4.799808373126808	2199.653735498236	37.926830445546244	119.81552679273904	14761.705948921815	0.090909090909090912	0.28925619834710742"
"33.54670639148091	-7.626672062543861	7496.671525365327	-110.40066747191628	-98.53164977572519	5354.521772014949	0.20867768595041322	0.21280991735537191"
"69.28094790755881	110.82842430477325	15322.190546810865	13.632371853416288	45.15801725859487	4113.530372935123	0.18388429752066116	0.17975206611570249"
"-137.51493797696065	136.6452876513947	12000.924046299344	106.56685053452935	86.00606372072906	14614.209629583916	0.17355371900826447	0.18181818181818182"
))

(defparameter *training* (list
"-57.03373327528257	108.13087898530961	12594.6945	58.647823612699625	-60.793057227368806	15290.132838589296	0.23553719008264462	0.17975206611570249"
"45.93574199369891	65.18637708844034	14019.713	93.82063849110173	-50.56044824659632	5415.1191612899465	0.256198347107438	0.13223140495867769"
"46.98458388781967	-84.50253616755111	8013.992	-34.15522842571828	-79.65499594158088	16795.692398827974	0.1962809917355372	0.25"
"138.61890886082818	-19.850473140327125	10792.229	-85.71788305762254	-4.205454130091283	15661.519149759057	0.16942148760330578	0.19214876033057851"
"-109.45300036671443	118.35804661650565	11742.8185	-102.11258293817338	-72.83488710404868	19047.6113086966	0.17768595041322313	0.18801652892561985"
"-50.08657928601161	-86.18168788592962	10498.295	-11.256087275294476	139.25990816080298	3987.877510045318	0.24380165289256198	0.2024793388429752"
"-81.05084481871168	-4.799808373126808	4690.0467	37.926830445546244	119.81552679273904	14761.705948921815	0.090909090909090912	0.28925619834710742"
"33.54670639148091	-7.626672062543861	8658.332	-110.40066747191628	-98.53164977572519	5354.521772014949	0.20867768595041322	0.21280991735537191"
"69.28094790755881	110.82842430477325	12378.284	13.632371853416288	45.15801725859487	4113.530372935123	0.18388429752066116	0.17975206611570249"
"-137.51493797696065	136.6452876513947	10954.873	106.56685053452935	86.00606372072906	14614.209629583916	0.17355371900826447	0.18181818181818182"
))

(defun initialize-record (&optional (dimensions '(101 101)))
  (setf *record* nil)
  (setf *outputs* (make-array dimensions :initial-element (list 0.0 0.0)))
  (setf *samples* (make-array dimensions :initial-element 0)))

(defparameter *scaling* 1.0)
(defun register (params)
  "robot_center,robot_dispersion,RV,D"
  (when *record*
    (setf (apply #'aref *outputs* *record*)
          (mapcar '+ (apply #'aref *outputs* *record*)
                   (list (third params)       ;RV
                         (fourth params))))    ;DP
    ;; (incf (first (apply #'aref *outputs* *record*)) (third params)) ;RV
    ;; (incf (second (apply #'aref *outputs* *record*)) (fourth params)) ;D
    (incf (apply #'aref *samples* *record*))
    )
  ;; (format t "~S,~S,~S,~S~%"
  ;;         (round (first params) (/ 1.0 (1- (first (array-dimensions *outputs*)))))
  ;;         (round (second params) (/ 1.0 (1- (second (array-dimensions *outputs*)))))
  ;;         (third params) (fourth params))
  (setf *record* (list (floor (first params) (/ *scaling* (1- (first (array-dimensions *outputs*)))))
                       (floor (second params) (/ *scaling* (1- (second (array-dimensions *outputs*))))))))

(defun print-output (out-file &optional (dimensions '(101 101)))
  (with-open-file (stream out-file :direction :output :if-exists :SUPERSEDE)
    (format stream "center,dispersion,Rendezvous,Deploy,counter")
    (dotimes (center (first DIMENSIONs))
      (dotimes (dispersion (second DIMENSIONs))
        (let ((val (apply #'aref *outputs* (list center dispersion)))
              (count (apply #'aref *samples* (list center dispersion))))
          (format stream "~%~D,~D,~1,4F,~1,4F,~D" center dispersion
                  (if (= (first val) 0) 0 (if (= count 0) 'NA (/ (first val) count)))
                  (if (= (second val) 0) 0 (if (= count 0) 'NA (/ (second val) count)))
                  count) ;mean tcoutput
          ;; (unless (or (= (first val) 0) (= (second val) 0) (= count 0))
          ;;   (format t "~%~D,~D,~S,~1,4F,~1,4F,~D" center dispersion val
          ;;           (if (= (first val) 0) 0 (if (= count 0) 'NA (/ (first val) count)))
          ;;           (if (= (second val) 0) 0 (if (= count 0) 'NA(/ (second val) count)))
          ;;           count))
          )
        )
      )
    )
  )

(defun generate-trial-set (dir)
  (format t "starting")
  (load (make-pathname :directory dir :name "D-RV" :type "lisp"))
  (load (make-pathname :directory dir :name "D-RV" :type "actr"))
  (format t "reading dataset...~%")
  (with-open-file (table (make-pathname :directory dir :name "large_set_permuted" :type "csv"))
    (read-line table nil)               ;heading
    (do ((line (read-line table nil)
               (read-line table nil)))
        ((null line))
      ;;      (mapcar #'read-from-string (split-string line))
      (incf *tot-trials*)
      (setq *experiment* (append *experiment* (list
                                               (subseq line (1+ (search (format nil "~C" #\tab) line)))))) ;skip the line number

      ;; divide training set in rv and dp configurations

      ;; (let ((value nil)
      ;;       (dp nil)
      ;;       (index 0))
      ;;   (multiple-value-setq (value index) (read-from-string line nil nil :start index))
      ;;   (multiple-value-setq (value index) (read-from-string line nil nil :start index))
      ;;   (multiple-value-setq (value index) (read-from-string line nil nil :start index))
      ;;   (multiple-value-setq (value index) (read-from-string line nil nil :start index))
      ;;   (multiple-value-setq (value index) (read-from-string line nil nil :start index))
      ;;   (multiple-value-setq (value index) (read-from-string line nil nil :start index))
      ;;   (multiple-value-setq (value index) (read-from-string line nil nil :start index))
      ;;   (multiple-value-setq (value index) (read-from-string line nil nil :start index)) ;read rv
      ;;   (multiple-value-setq (dp index) (read-from-string line nil nil :start index)) ;read dp
      ;;   (if (>= value dp)
      ;;       (setq *experiment-rv* (append *experiment-rv* (list (subseq line (1+ (search (format nil "~C" #\tab) line))))))
      ;;       (setq *experiment-dp* (append *experiment-dp* (list (subseq line (1+ (search (format nil "~C" #\tab) line))))))
      ;;       )
      ;;   )
      )
    )
  (format t "dataset contains ~S trials~%Shuffling dataset...~%" *tot-trials*)
  (setq *experiment* (shuffle *experiment*))
  (format t "Dataset shuffled (~S elements), starting simulation~%" (length *experiment*))
  (setq *experiment* (append *experiment* (last *experiment*))) ;duplicate the last
  (initialize-record)
  ;; (setq *tot-trials* 1)
  (with-open-file (log (make-pathname :directory dir :name "actr_log" :type "csv") :direction :output :if-exists :SUPERSEDE)
    (format log "run_num,trial_num,robot_x,robot_y,robot_scale,RV.Value,DP.Value,RV.Estimate,DP.Estimate~%")
    (loop for i from 0 to (1- *tot-trials*) by *exp-length* do
         (if (integerp (/ i (/ *tot-trials* 10))) (format t "~%"))
         (if (integerp (/ i (/ *tot-trials* 100))) (format t "-"))
         (setf *record* nil)
         (let ((exp (subseq *experiment* i (+ i *exp-length* 1)))) ;leave one extra trial at the end to compute the last output
           (mapcar #'register (run-experiment *runs* (append *training_variance* exp) log nil (length *training_variance*) (length exp)))
           )
         )
  )
  (print-output (make-pathname :directory dir :name "training_set" :type "csv"))
;;  (with-open-file (end (make-pathname :directory dir :name "done" :type "txt") :direction :output :if-exists :SUPERSEDE) (format end "a"))
  )

(defun split-string (str &key (separators '(#\space #\tab)))
  "splits a string containing spaces or tabs in a list of spaceless strings"
  (let ((pos (reduce (lambda (a b) (or a b)) (append ;apply the special operator OR to the list
                                              (mapcar #'(lambda (x) (position x str)) separators) ;apply the function position to each separator in the list
                                              (list -1)         ;default value, in case no separator has been found
                                              ))))
    (cond ((= pos -1) str)
    ((> pos -1) (flatten (list
           (subseq str 0 pos)
           (split-string (subseq str (+ pos 1) (length str)) :separators separators)))))))

(defun shuffle (seq &optional (k (length seq) k-supplied-p))
  "shuffle all of SEQ. Stop when k elements have been shuffled.
This generalizes Knuth's algorithm"	; <kwwtx9ojwo.fsf@merced.netfonds.no>
  (let ((n (length seq)))
    (dotimes (i k seq)
      (rotatef (elt seq i)(elt seq (+ i (random (- n i))))))))
