;;; Copyright 2013-2014 Carnegie Mellon University.

;;; Generate-file-with-actr-estimates is the main entry point. It is called with
;;; at least one argument, the directory from which to read the data files, and
;;; into which to write its results. The names of the input data files, and output file,
;;; have defaults, but these can be overriden with keyword arguments if desired.
;;;
;;; Generate-file-with-actr-estimates first reads two tab separated data files, one with
;;; the data to act on, and the other with training data. Both are read into vectors of
;;; trial objects. The trial objects do not currently capture the wall data, only the
;;; robot swarm data, the wall data in the files being discarded. It partitions the
;;; non-trial data into randomly selected buckets of size BUCKET-SIZE, by default 100. It
;;; then runs the ACT-R model over each bucket REPETITION times, by default 50.
;;;
;;; For each of the repetitions over a bucket it first resets ACT-R, and then adds chunks
;;; to declarative memory correspnding to the training data, each training datum mapping a
;;; centroid, dispersion pair to the coverage for a rendezous and a deploy decision.
;;; Note that the same training data is used for every bucket, and for every repetition
;;; within a bucket. After the training data is added to the DM, each of the trials in
;;; the bucket is run through the ACT-R model, which uses the LINEAR-SIMILARITY function
;;; and blending to arrive at expectd values for the coverage for a rendezvous or a
;;; deploy decision. The model learns the ground truth for the value of each decision
;;; it makes; it does not learn the ground truth for the decisions foregone. The learning
;;; lasts only for the current repetition of the current bucket, and is then discarded.
;;;
;;; The results are written to a CSV file, one row per trial, per repetition, per bucket.
;;; The rows contain indecies indicating the bucket, repetition and trial; the x,y
;;; position of the centroid of the robot swarm, and its dispersion, as well as the
;;; computed distance from the center of the field of that centroid normalized to the
;;; range zero to one, and the dispersion normalized to the same range; the ground truth
;;; rendezvous and deploy coverages; ACT-R's estimated rendezvous and deploy coverages;
;;; and some ratios between those coverage numbers.

(defconstant +default-data-file+ "swarm-data.tsv")
(defconstant +default-training-data-file+ "training-data.tsv")
(defconstant +default-output-file+ "actr-results.csv")

(defconstant +default-bucket-size+ 100)
(defconstant +default-repetitions+ 50)

(defconstant +actr-parameters+ '(:ans 0.75 
                                 :tmp 0.7
                                 :mp 2.5
                                 :bll 0.5
                                 :rt -10.0
                                 :blc 5
                                 :lf 0.25
                                 :ol 1
                                 :md -2.5))

(defconstant +centroid-normalization-factor+ (/ 1.0 140))
(defconstant +dispersion-normalization-factor+ (/ 1.0 14000))

(defconstant +actr-timeout+ 10.0)

(defconstant +header+
  "set,rep,trial,x,y,cent,disp,norm.disp,RV.grnd,DP.grnd,RV.actr,DP.actr,SEL,SEL.grnd-SEL.acrt,SEL.grnd-NSEL.act,SEL.grnd-NSEL.grnd")


(unless (member :act-r-6.0 *features*)
  (error "ACT-R must be loaded before loading, or even compiling, this file"))

(defclass trial ()
  ;; The field in which the robot swarms move is 300 x 300 units,
  ;; the coordinates running from -150 to +150.
  ;;
  ;; It's not entirely clear what the units of dispersion are, but it is
  ;; presumably something to do with variances of their positions, and has
  ;; values in the range 0 to 1,400.
  ((x :initarg :x :reader robot-x
      :documentation "x position of the centroid of the robots")
   (y :initarg :y :reader robot-y
      :documentation "y position of the centroid of the robots")
   (normalized-distance :accessor normalized-distance
                        :documentation "distance of centroid from center of the field, normalized to the range 0 to 1")
   (dispersion :initarg :dispersion :reader dispersion
               :documentation "the dispersion of the swarm of robots")
   (normalized-dispersion :accessor normalized-dispersion
                          :documentation "the dispersion of the swarm of robots, normalized to the range 0 to 1")
   (rendezvous :initarg :rendezvous :reader rendezvous
    :documentation "ground truth value for coverage fraction after rendezvous operation")
   (deploy :initarg :deploy :reader deploy
    :documentation "ground truth value for coverage fraction after deploy operation")))

(defmethod print-object ((object trial) stream)
  (format stream "#<trial~@{ ~,2F~}>"
          (robot-x object)
          (robot-y object)
          (normalized-distance object)
          (dispersion object)
          (normalized-dispersion object)
          (rendezvous object)
          (deploy object))
  object)

(defun make-trial (x y dispersion rendezvous deploy)
  (let ((result (make-instance 'trial :x x :y y :dispersion dispersion
                                      :rendezvous rendezvous :deploy deploy)))
    (setf (normalized-distance result)
          (* (sqrt (+ (expt (robot-x result) 2) (expt (robot-y result) 2)))
             +centroid-normalization-factor+))
    (setf (normalized-dispersion result)
          (* (dispersion result) +dispersion-normalization-factor+))
    result))

(defmacro with-open-file-preserved ((svar file &rest keys &key &allow-other-keys) &body body)
  ;; When using with-open-file :direction :output :if-exists :supersede, if the body is
  ;; aboted, the new file being written is discarded. This is for use when we'd prefer to
  ;; keep the partially written file.
  `(let ((,svar (open ,file :direction :output :if-exists :supersede ,@keys)))
     (unwind-protect
          (progn ,@body)
       (finish-output ,svar)
       (close ,svar))))

(defvar *training-data*)
(defvar *output*)
(defvar *show-parameters*)

(defun generate-file-with-actr-estimates (directory &key (data-file +default-data-file+)
                                                         (training-data-file +default-training-data-file+)
                                                         (output-file +default-output-file+)
                                                         (bucket-size +default-bucket-size+)
                                                         (repetitions +default-repetitions+)
                                                         (shuffle :once)
                                                         (actr-parameters nil))
  ;; If supplied, actr-parameters should be alternating keywords and values, and these
  ;; values will override those given in +actr-parameters+.
  ;; If bucket-size is nil the data will not be partitioned.
  (check-type shuffle (member :never :once :always))
  (setf directory (merge-pathnames directory))
  (let ((data (read-data (merge-pathnames data-file directory) :notice-stream t))
        (*training-data* (read-training-data (merge-pathnames training-data-file directory)
                                             :notice-stream t))
        (*show-parameters* t)
        (parameters (copy-list +actr-parameters+)))
    (labels ((report (file trials)
               (loop for tr across trials
                     minimize (normalized-distance tr) into min-c
                     maximize (normalized-distance tr) into max-c
                     minimize (normalized-dispersion tr) into min-d
                     maximize (normalized-dispersion tr) into max-d
                     finally (format t "~A: centroid [~,4F,~,4F]; dispersion [~,4F,~,4F]~%"
                                     file min-c max-c min-d max-d))))
      (report training-data-file *training-data*)
      (report data-file data))
    (unless bucket-size
      (setf bucket-size (length data)))
    (when (eq shuffle :once)
      (setf data (shuffle data)))
    (loop for (key value) on actr-parameters by #'cddr
          do (setf (getf parameters key) value))
    (define-swarm-model parameters)
    (with-open-file-preserved (*output* (merge-pathnames output-file directory))
      (format *output* "~A~%" +header+)
      (loop for i from 0 to (- (length data) bucket-size) by bucket-size
            for bucket = (subseq data i (+ i bucket-size))
            for bucket-number from 1
            do (note-progress bucket-number)
            do (loop for repetition from 1 to repetitions
                     when (eq shuffle :always) do (setf bucket (shuffle bucket))
                     do (run-model bucket bucket-number repetition)))
      (truename *output*))))

(defun read-data-from-string (string)
  ;; STRING should be a sequence of readable, printed representations of Lisp objects,
  ;; typically numbers, separated by whitespace, likely tabs. Returns a list of those
  ;; objects.
  (with-input-from-string (s string)
    (loop collect (or (read s nil nil) (loop-finish)))))

(defun map-data-file (fn file &key (skip-first-line t) (notice-stream nil) (filter #'identity))
  ;; Returns an array of the results of calling FN on the list of numbers
  ;; parsed from each line of the file.
  (with-open-file (file-stream file :direction :input)
    (when notice-stream
      (format notice-stream "~&Reading from ~A ..." (truename file-stream)))
    (when skip-first-line
      (read-line file-stream))
    (loop with result = (make-array 20 :fill-pointer 0 :adjustable t)
          do (vector-push-extend
              (apply fn
                     (read-data-from-string
                      (or (funcall filter (read-line file-stream nil nil)) (loop-finish))))
              result
              10000)
          finally (progn
                    (when notice-stream
                      (format notice-stream " ~:D rows read.~%" (length result)))
                    (return result)))))

(defun read-data (file &key  notice-stream)
  (map-data-file #'(lambda (i1 i2 x y ds i3 i4 i5 r d)
                     (declare (ignore i1 i2 i3 i4 i5))
                     (make-trial x y ds r d))
                 file
                 :notice-stream notice-stream))

(defun read-training-data (file &key notice-stream)
  (map-data-file #'(lambda (x y ds i1 i2 i3 r d)
                     (declare (ignore i1 i2 i3))
                     (make-trial x y ds r d))
                 file
                 :skip-first-line nil
                 :notice-stream notice-stream))

(defun shuffle (sequence)
  (loop for i from (length sequence) above 0
        do (rotatef (elt sequence (- i 1))  (elt sequence (random i))))
  sequence)

(defvar *chunk-index*)

(defun run-model (bucket bucket-number repetition)
  (let ((*chunk-index* 0))
    (initialize-model)
    (loop for trial across bucket
          for i from 1
          do (multiple-value-bind (ren dep) (run-model-once trial)
               (multiple-value-bind (sel -sel.actr -nsel.actr -nsel.grnd)
                   (if (< ren dep)
                       (values 'deploy
                               (- (deploy trial) dep)
                               (- (deploy trial) ren)
                               (- (deploy trial) (rendezvous trial)))
                       (values 'rendezvous
                               (- (rendezvous trial) ren)
                               (- (rendezvous trial) dep)
                               (- (rendezvous trial) (deploy trial))))
                 (format *output* "~3@{~D,~}~9@{~,4F,~}~A,~@{~,4F~^,~}~%"
                         bucket-number
                         repetition
                         i
                         (robot-x trial)
                         (robot-y trial)
                         (normalized-distance trial)
                         (dispersion trial)
                         (normalized-dispersion trial)
                         (rendezvous trial)
                         (deploy trial)
                         ren
                         dep
                         sel
                         -sel.actr
                         -nsel.actr
                         -nsel.grnd))))))

(defun note-progress (current)
  (cond ((eql current 1) (format t "~&."))
        ((zerop (mod current 50)) (format t "~4D~%" current))
        (t (format t "."))))

(defun initialize-model ()
  (reset)
  (when *show-parameters*
    (sgp)
    (terpri)
    (setf *show-parameters* nil))
  (loop for trial across *training-data*
        do (mod-chunk-fct (first (add-dm-fct (make-chunk trial)))
                          `(rendezvous ,(rendezvous trial) deploy ,(deploy trial)))))

(defparameter *nnn* 0)

(defun run-model-once (trial)
  (goal-focus-fct (first (define-chunks-fct (make-chunk trial))))
  (run +actr-timeout+)
  (multiple-value-bind (ren dep)
      (no-output (values (chunk-slot-value-fct (first (buffer-chunk goal)) 'rendezvous)
                         (chunk-slot-value-fct (first (buffer-chunk goal)) 'deploy)))
    ;; Ties are exceedingly unlikey, but if one happens, perturb one of the values, chosen
    ;; randomly, randomly up or down, to break the tie.
    (when (= ren dep)
      (let ((increment (* 0.00001 (- (* 2 (random 2)) 1))))
        (if (zerop (random 2))
            (incf ren increment)
            (incf dep increment))))
    ;; Whichever choice the model made, give it feedback of the ground truth result
    ;; for that choice.
    (mod-focus-fct (if (> ren dep)
                       (list 'rendezvous (rendezvous trial))
                       (list 'deploy (deploy trial))))
    ;(format t "~& #~D# -------------- ~A, ~A, ~A, ~A~2%" (incf *nnn*) (rendezvous trial) (deploy trial) ren dep)
    ;(dm)
    (values ren dep)))

(defun make-chunk (trial)
  `((,(intern (format nil "DECISION-~D" (incf *chunk-index*)))
      isa decision
      robotc ,(normalized-distance trial)
      robotd ,(normalized-dispersion trial)
      obstaclec dont-care
      obstacled dont-care
      distance dont-care)))

;;; ACT-R model

(defun linear-similarity (x y)
  (cond ((and (integerp x) (integerp y)) (/ (- (abs (- x y))) 10.0))
        ((and (numberp x) (numberp y)) (- (abs (- x y))))
        ((or (eq x 'dont-care) (eq y 'dont-care)) 0.0)
        (t (no-output (first (sgp :md))))))

(defun define-swarm-model (parameters)
  (clear-all)
  (define-model-fct 'deploy-rendezvous `(

(sgp :esc t
     :sim-hook linear-similarity
     ,@parameters
     :v nil
     :blt nil
     :trace-detail low
     :style-warnings nil)

;;; Representation of problem and outcome estimation
;;; centrality of robots [0,1], dispersion of robots [0,1], centrality of obstacles [0,1],
;;; dispersion of obstacles [0,1], and distance from robot to obstacle centroid [0,1]
;;; Holds evaluation of rendezvous and deploy

(chunk-type decision robotc robotd obstaclec obstacled distance rendezvous deploy)
(chunk-type dontcare)
(chunk-type null)

;;; Initial chunks

(add-dm (dont-care isa dontcare)
        (null isa null))

(declare-buffer-usage goal decision)

;;; generate each outcome estimation

(p inference-initiate
=goal>
      isa decision
      robotc =robotc
      robotd =robotd
      obstaclec =obstaclec
      obstacled =obstacled
      distance =distance
      rendezvous nil
      deploy nil
?blending>
  state free
==>
+blending>
      isa decision
      robotc =robotc
      robotd =robotd
      obstaclec =obstaclec
      obstacled =obstacled
      distance =distance
=goal>
  deploy 0
)

(p inference
=goal>
      isa decision
      deploy 0
=blending>
      isa decision
      robotc =robotc
      robotd =robotd
      obstaclec =obstaclec
      obstacled =obstacled
      distance =distance
      rendezvous =rendezvous
      deploy =deploy
?blending>
   state free
==>
;!eval! (format t "Blending SUCCEEDED ~s, Rendezvous: ~s, Deploy: ~s~%" =blending =rendezvous =deploy)
=blending> null ;save a copy in DM with original expectations
=goal>
      rendezvous =rendezvous
      deploy =deploy
)

;; this should never get fired
(p nil-values
=goal>
   isa decision
   deploy 0
?blending>
   state free
=blending>
      isa decision
      rendezvous nil
      deploy nil
==>
!eval! (format t "Blending NIL VALUES, setting values to NIL~%")
!eval! (buffer-chunk blending)
=blending>
=goal>
  rendezvous nil
  deploy nil
)

(p aoe
=goal>
   isa decision
   deploy 0
?blending>
  state error
==>
!bind! =rand (+ (act-r-random 1.0) 2)
;!eval! (format t "Blending FAILED, setting values to random~%")
=goal>
  deploy =rand
)

(p guess-rv
=goal>
  isa decision
  deploy =rand
  >= deploy 2.0
  < deploy 3.0
!eval! (< =rand 2.5)
==>
!eval! (format t "Guessing Rendezvous~%")
=goal>
  rendezvous	0.21
  deploy		0.19
)

(p guess-dp
=goal>
  isa decision
  deploy =rand
  >= deploy 2.0
  < deploy 3.0
!eval! (>= =rand 2.5)
==>
!eval! (format t "Guessing Deploy~%")
=goal>
  rendezvous	0.19
  deploy		0.21)

))) ; end of (defun define-swarm-model


;; Local variables:
;; fill-column: 90
;; End:
