import datetime as dt
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.stats as st

scale = 500

adata = pd.read_csv('actr-results-human-2014-10-14a.csv')
plt.title('Current model run on data used in human experiments')
trials = max(adata['trial'])
adata['correct'] = [1 if (rg < dg) == (ra < da) else 0
                    for rg, dg, ra, da in zip(
                        adata['RV.grnd'], adata['DP.grnd'],
                        adata['RV.actr'], adata['DP.actr'])]
adata['groundTruthRendezvous'] = [1 if rg >= dg else 0
                                  for rg, dg in zip(
                                    adata['RV.grnd'], adata['DP.grnd'])]                                    
summary = adata.groupby('trial').mean()
tdata = pd.read_csv('corrected-training-data.tsv', sep='\t', names="x y d i1 i2 i3 rn dp".split())
tdata['cent'] = np.sqrt(tdata['x']*tdata['x'] + tdata['y']*tdata['y']) / 140
tdata['disp'] = tdata['d'] / 14000
tdata['groundTruthRendezvous'] = [1 if rg >= dg else 0
                                  for rg, dg in zip(
                                    tdata['rn'], tdata['dp'])]                                    
xdata = summary.index + 10
hdata = pd.read_csv('human-results.csv')

def makePlot(fractionCorrect, secondFraction, plotTitle):
    plt.scatter(summary['norm.disp'], summary['cent'], s=scale*fractionCorrect['correct'], c=summary['groundTruthRendezvous'], alpha=0.25)
    if secondFraction:
        plt.scatter(summary['norm.disp'], summary['cent'], s=scale*secondFraction['correct'], c='gray', alpha=0.25)
    plt.scatter(tdata['disp'],tdata['cent'],  marker='+', s=scale/4, label='training', c=tdata['groundTruthRendezvous'])
    plt.axis([0, 2, 0, 1.4])
    for x, y, i in zip(summary['norm.disp'], summary['cent'], range(11, 61)):
        plt.text(x, y, str(i), ha='center', va='center', size='smaller')
    plt.title(plotTitle)
    plt.xlabel("dispersion")
    plt.ylabel("eccentricity")
    plt.text(10, -0.1, str(dt.date.today()))
    xloc = 1.9
    plt.scatter([xloc]*4, [1.3, 1.2, 1.1, 1.0],
                s=[scale, scale*0.75, scale*0.5, scale*0.25], c='gray', alpha=0.25)
    plt.text(xloc, 1.3, '100%', ha='center', va='center', size='smaller')
    plt.text(xloc, 1.2, '75%', ha='center', va='center', size='smaller')
    plt.text(xloc, 1.1, '50%', ha='center', va='center', size='smaller')
    plt.text(xloc, 1.0, '25%', ha='center', va='center', size='smaller')
    plt.show()

#makePlot(summary, None, 'Current model run on data used in human experiments')
#makePlot(hdata, None, 'Human data')
makePlot(summary, hdata, 'Human subjects and ACT-R model results compared')
    
