import datetime as dt
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.stats as st
import scipy.optimize as so

def sigmoid(x, c, k, x0, y0):
    return c / (1 + np.exp(-k*(x - x0))) + y0

mdata = pd.read_csv('actr-results-human-2014-10-14a.csv')
hdata = pd.read_csv('human-results.csv')
trials = max(mdata['trial'])
mdata['rendezvous'] = [1 if ra >= da else 0 for ra, da in zip(mdata['RV.actr'], mdata['DP.actr'])]
mdata['difference'] = mdata['RV.grnd'] - mdata['DP.grnd']
summary = mdata.groupby('trial').mean()
hdata['rendezvous'] = [hdata['correct'][i] if (summary['RV.grnd'][i+1] > summary['DP.grnd'][i+1]) else 1 - hdata['correct'][i] for i in range(0, 50)]
plt.scatter(summary['difference'], hdata['rendezvous'], c='orange', label='human')
plt.scatter(summary['difference'], summary['rendezvous'], c='black', label='model')
plt.axis([-0.15, 0.15, 0, 1])
plt.grid(True)
plt.ylabel("fraction of participants that chose rendezvous")
plt.xlabel("ground truth rendezvous coverage - deploy coverage")
plt.text(10, -0.1, str(dt.date.today()))
plt.title("Human subjects and ACT-R model")
plt.legend(loc=4)
((c, k, x0, y0), ignore) = so.curve_fit(sigmoid, summary['difference'], summary['rendezvous'], (1, 14, 0, 0.15))
print(c, k, x0, y0)
plt.plot(np.arange(-0.15, 0.16, 0.01), [sigmoid(x, c, k, x0, y0) for x in np.arange(-0.15, 0.16, 0.01)], c='black')
((c, k, x0, y0), ignore) = so.curve_fit(sigmoid, summary['difference'], [hdata['rendezvous'][i] for i in range(0, 50)], (1, 14, 0, 0.15))
print(c, k, x0, y0)
plt.plot(np.arange(-0.15, 0.16, 0.01), [sigmoid(x, c, k, x0, y0) for x in np.arange(-0.15, 0.16, 0.01)], c='orange')
plt.show()



