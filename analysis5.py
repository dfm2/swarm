import datetime as dt
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.stats as st


def printDifferences(name, values1, values2):
    assert len(values1) == len(values2)
    squared = sum((v1 - v2)**2 for v1, v2 in zip(values1, values2)) / len(values1)
    sqrted = np.sqrt(squared)
    absed = sum(abs(v1 - v2) for v1, v2 in zip(values1, values2)) / len(values1)
    print("  {}: {:.2f}, {:.2f}, {:.2f}".format(name, squared, sqrted, absed))

mdata = pd.read_csv("actr-results-human-2014-10-14a.csv")
hdata = pd.read_csv('human-results.csv')
trials = max(mdata['trial'])
mdata['correct'] = [1 if (rg < dg) == (ra < da) else 0
                    for rg, dg, ra, da in zip(
                            mdata['RV.grnd'], mdata['DP.grnd'],
                            mdata['RV.actr'], mdata['DP.actr'])]
summary = mdata.groupby('trial').mean()
xdata = summary.index + 10
#slope, intercept , ignore1, ignore2, ignoe3  = st.linregress(xdata, summary['correct'])
plt.plot(xdata, hdata['correct'], 'g-', marker='.', label='Human subjects')
plt.plot(xdata, summary['correct'], 'k-', marker='.', label='ACT-R model')
plt.axis([11, trials+10, 0, 1])
plt.grid(True)
plt.ylabel("fraction correct")
plt.xlabel("trial")
#plt.plot([11, trials + 11], [intercept + 10*slope, (trials + 10)*slope + intercept], 'g-')
#slope *= trials + 10
plt.text(10, -0.1, str(dt.date.today()))
plt.legend(loc=0)
plt.title('Fraction correct by trial')
print("Average differences:")
printDifferences("ACT-R model v. human", summary['correct'], hdata['correct'])
printDifferences("ACT-R model v. ground truth(1)", summary['correct'], [1]*len(summary['correct']))
printDifferences("human v. ground truth(1)", hdata['correct'], [1]*len(hdata['correct']))
plt.show()

# trials = max(data['trial'])
# xdata = data.index + 11
# slope, intercept , ignore1, ignore2, ignoe3  = st.linregress(xdata, data['correct'])
# plt.axis([11, trials+11, 0, 1.03])
# plt.grid(True)
# plt.ylabel("fraction correct")
# plt.xlabel("trial")
# plt.plot([11, trials + 11], [intercept + 10*slope, (trials + 10)*slope + intercept], 'g-')
# slope *= trials + 10
# plt.title("{}\n(slope: {:.2f})".format('Human results', slope))
# plt.text(10, -0.1, str(dt.date.today()))
# plt.show()

