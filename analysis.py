import datetime as dt
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.stats as st

plotId = None

def makePlot(filename, plotname, linestyle='bo', threshold=None, set=None):
    global plotId
    sys.stdout.write(plotname)
    sys.stdout.flush()
    data = pd.read_csv(filename)
    trials = max(data['trial'])
    if set:
        data = data[data['set'] == set]
    if threshold:
        data = data[abs(data['SEL.grnd-NSEL.grnd']) >= threshold]
    data['correct'] = [1 if (rg < dg) == (ra < da) else 0
                       for rg, dg, ra, da in zip(
                               data['RV.grnd'], data['DP.grnd'],
                               data['RV.actr'], data['DP.actr'])]
    summary = data.groupby('trial').mean()
    xdata = summary.index + 10
    slope, intercept , ignore1, ignore2, ignoe3  = st.linregress(xdata, summary['correct'])
    plt.plot(xdata, summary['correct'], linestyle, marker='.')
    plt.axis([11, trials+11, 0, 1.03])
    plt.grid(True)
    plt.ylabel("fraction correct")
    plt.xlabel("trial")
    plt.plot([11, trials + 11], [intercept + 10*slope, (trials + 10)*slope + intercept], 'g-')
    slope *= trials + 10
    if threshold:
        #plt.title("{}\n(threshold: {:.2f}, trials above threshold: {}, slope: {:.2f})".format(plotname, threshold, len(xdata), slope))
        plt.title("{}\n(threshold: {:.2f}, slope: {:.2f})".format(plotname, threshold, slope))
    else:
        plt.title("{}\n(slope: {:.2f})".format(plotname, slope))
    plt.text(10, -0.1, str(dt.date.today()))
    if plotId:
        plt.savefig("plot-{}.png".format(plotId))
        plotId += 1
    else:
        plt.show()
    sys.stdout.write("...done\n")
    sys.stdout.flush()

# makePlot('actr-results-human-2014-08-30.csv', "ACT-R model run on the data used for human experiment", linestyle='b-')
# makePlot('actr-results-human-2014-08-30.csv', "ACT-R model run on the data used for human experiment")
# makePlot('actr-results-human-2014-08-30.csv', "ACT-R model run on the data used for human experiment", threshold=0.01)
# makePlot('actr-results-human-2014-08-30.csv', "ACT-R model run on the data used for human experiment", threshold=0.05)
# makePlot('actr-results-human-2014-08-30.csv', "ACT-R model run on the data used for human experiment", threshold=0.1)

# makePlot('actr-results-uniform-2014-08-31.csv', "ACT-R model, 1000x100 uniform") 
# makePlot('actr-results-uniform-2014-08-31.csv', "ACT-R model, 1000x100 uniform", threshold=0.01)
# makePlot('actr-results-uniform-2014-08-31.csv', "ACT-R model, 1000x100 uniform", threshold=0.05)
# makePlot('actr-results-uniform-2014-08-31.csv', "ACT-R model, 1000x100 uniform", threshold=0.1)

# 148, 541, 810
# makePlot('actr-results-uniform-2014-08-31.csv', "ACT-R model, x100 uniform, set 148", set=148)
# makePlot('actr-results-uniform-2014-08-31.csv', "ACT-R model, x100 uniform, set 541", set=541)
# makePlot('actr-results-uniform-2014-08-31.csv', "ACT-R model, x100 uniform, set 810", set=810)

# makePlot('actr-results-uniform-2014-08-31.csv', "ACT-R model, x100 uniform, set 148", set=148, threshold=0.01)
# makePlot('actr-results-uniform-2014-08-31.csv', "ACT-R model, x100 uniform, set 148", set=148, threshold=0.05)
# makePlot('actr-results-uniform-2014-08-31.csv', "ACT-R model, x100 uniform, set 148", set=148, threshold=0.1)

# makePlot('actr-results-uniform-big-buckets-2014-08-31.csv', "ACT-R model, 100x1000 uniform")
# makePlot('actr-results-uniform-big-buckets-2014-08-31.csv', "ACT-R model, 100x1000 uniform", threshold=0.01)
# makePlot('actr-results-uniform-big-buckets-2014-08-31.csv', "ACT-R model, 100x1000 uniform", threshold=0.05)
# makePlot('actr-results-uniform-big-buckets-2014-08-31.csv', "ACT-R model, 100x1000 uniform", threshold=0.1)
# makePlot('actr-results-uniform-big-buckets-2014-08-31.csv', "ACT-R model, x1000 uniform, set=34", set=34)
# makePlot('actr-results-uniform-big-buckets-2014-08-31.csv', "ACT-R model, x1000 uniform, set=34", set=34, threshold=0.01)
# makePlot('actr-results-uniform-big-buckets-2014-08-31.csv', "ACT-R model, x1000 uniform, set=34", set=34, threshold=0.05)
# makePlot('actr-results-uniform-big-buckets-2014-08-31.csv', "ACT-R model, x1000 uniform, set=34", set=34, threshold=0.1)

makePlot('actr-results.csv', "ACT-R model, human data, corrrected, :ans 0.75, :tmp 0.7, :mp 2.5", linestyle='b-')

data = pd.read_csv('human-results.csv')
trials = max(data['trial'])
xdata = data.index + 11
slope, intercept , ignore1, ignore2, ignoe3  = st.linregress(xdata, data['correct'])
plt.plot(xdata, data['correct'], 'b-', marker='.')
plt.axis([11, trials+11, 0, 1.03])
plt.grid(True)
plt.ylabel("fraction correct")
plt.xlabel("trial")
plt.plot([11, trials + 11], [intercept + 10*slope, (trials + 10)*slope + intercept], 'g-')
slope *= trials + 10
plt.title("{}\n(slope: {:.2f})".format('Human results', slope))
plt.text(10, -0.1, str(dt.date.today()))
plt.show()

