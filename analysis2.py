import datetime as dt
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.stats as st


#adata = pd.read_csv('actr-results-human-2014-08-30.csv')
adata = pd.read_csv('actr-results.csv')
trials = max(adata['trial'])
adata['correct'] = [1 if (rg < dg) == (ra < da) else 0
                    for rg, dg, ra, da in zip(
                        adata['RV.grnd'], adata['DP.grnd'],
                        adata['RV.actr'], adata['DP.actr'])]
summary = adata.groupby('trial').mean()
xdata = summary.index + 10
plt.plot(xdata, summary['correct'], marker='.', label="'current' model")
pdata = pd.read_csv('actr-results-used-in-paper.csv')
plt.plot(xdata, pdata['correct'], 'r-', marker='.', label="model used in AAAI paper")
plt.axis([10, trials+10, 0, 1.03])
plt.xticks(range(10, 61))
plt.grid(True)
plt.legend(loc=10)
plt.ylabel("fraction correct")
plt.xlabel("trial")
plt.text(10, -0.1, str(dt.date.today()))
plt.show()
